# tartube

A GUI front-end for youtube-dl, partly based on youtube-dl-gui and written in Python 3 / Gtk 3

https://github.com/axcore/tartube

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/tartube/tartube.git
```

